# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [uplink.proto](#uplink.proto)
    - [DeviceMetadata](#.DeviceMetadata)
    - [GatewayMetadata](#.GatewayMetadata)
    - [WithMetadataFromDevice](#.WithMetadataFromDevice)
    - [WithMetadataFromGateway](#.WithMetadataFromGateway)
    - [WithMetadataToDevice](#.WithMetadataToDevice)
    - [WithMetadataToGateway](#.WithMetadataToGateway)
  
    - [Bandwidth](#.Bandwidth)
    - [CodeRate](#.CodeRate)
    - [Modulation](#.Modulation)
    - [SpreadingFactor](#.SpreadingFactor)
  
  
  

- [Scalar Value Types](#scalar-value-types)



<a name="uplink.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## uplink.proto
file level komment


<a name=".DeviceMetadata"></a>

### DeviceMetadata



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| deveui | [string](#string) |  |  |
| latitude | [float](#float) |  |  |
| longitude | [float](#float) |  |  |
| devaddr | [string](#string) |  |  |
| nwkskey | [string](#string) |  |  |
| appskey | [string](#string) |  |  |






<a name=".GatewayMetadata"></a>

### GatewayMetadata



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| macaddress | [string](#string) |  |  |
| latitude | [float](#float) |  |  |
| longitude | [float](#float) |  |  |






<a name=".WithMetadataFromDevice"></a>

### WithMetadataFromDevice



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| payload | [string](#string) |  |  |
| frequency | [uint32](#uint32) |  |  |
| modulation | [Modulation](#Modulation) |  |  |
| bandwidth | [Bandwidth](#Bandwidth) |  |  |
| spreadingfactor | [SpreadingFactor](#SpreadingFactor) |  |  |
| coderate | [CodeRate](#CodeRate) |  |  |
| payloadsize | [uint32](#uint32) |  |  |
| txpower | [uint32](#uint32) |  |  |
| deveui | [string](#string) |  |  |






<a name=".WithMetadataFromGateway"></a>

### WithMetadataFromGateway



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| txpower | [uint32](#uint32) |  |  |
| modulation | [Modulation](#Modulation) |  |  |
| bandwidth | [Bandwidth](#Bandwidth) |  |  |
| spreadingfactor | [SpreadingFactor](#SpreadingFactor) |  |  |
| coderate | [CodeRate](#CodeRate) |  |  |
| payload | [string](#string) |  |  |
| payloadsize | [uint32](#uint32) |  |  |
| frequency | [uint32](#uint32) |  |  |
| gweui | [string](#string) |  |  |






<a name=".WithMetadataToDevice"></a>

### WithMetadataToDevice



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| modulation | [Modulation](#Modulation) |  |  |
| bandwidth | [Bandwidth](#Bandwidth) |  |  |
| spreadingfactor | [SpreadingFactor](#SpreadingFactor) |  |  |
| coderate | [CodeRate](#CodeRate) |  |  |
| payload | [string](#string) |  |  |
| payloadsize | [uint32](#uint32) |  |  |
| frequency | [uint32](#uint32) |  |  |
| rssi | [float](#float) |  |  |
| snr | [float](#float) |  |  |
| snr_min | [float](#float) |  |  |
| snr_max | [float](#float) |  |  |






<a name=".WithMetadataToGateway"></a>

### WithMetadataToGateway



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| payload | [string](#string) |  |  |
| frequency | [uint32](#uint32) |  |  |
| modulation | [Modulation](#Modulation) |  |  |
| bandwidth | [Bandwidth](#Bandwidth) |  |  |
| spreadingfactor | [SpreadingFactor](#SpreadingFactor) |  |  |
| coderate | [CodeRate](#CodeRate) |  |  |
| payloadsize | [uint32](#uint32) |  |  |
| rssi | [float](#float) |  |  |
| snr | [float](#float) |  |  |
| snr_min | [float](#float) |  |  |
| snr_max | [float](#float) |  |  |





 


<a name=".Bandwidth"></a>

### Bandwidth


| Name | Number | Description |
| ---- | ------ | ----------- |
| BW_UNDEFINED | 0 |  |
| BW_500KHZ | 1 |  |
| BW_250KHZ | 2 |  |
| BW_125KHZ | 3 |  |
| BW_62K5HZ | 4 |  |
| BW_31K2HZ | 5 |  |
| BW_15K6HZ | 6 |  |
| BW_7K8HZ | 7 |  |



<a name=".CodeRate"></a>

### CodeRate


| Name | Number | Description |
| ---- | ------ | ----------- |
| CR_UNDEFINED | 0 |  |
| CR_LORA_4_5 | 1 |  |
| CR_LORA_4_6 | 2 |  |
| CR_LORA_4_7 | 3 |  |
| CR_LORA_4_8 | 4 |  |



<a name=".Modulation"></a>

### Modulation


| Name | Number | Description |
| ---- | ------ | ----------- |
| MOD_UNDEFINED | 0 | undefined

de mégis defined |
| MOD_LORA | 1 |  |
| MOD_FSK | 2 |  |



<a name=".SpreadingFactor"></a>

### SpreadingFactor


| Name | Number | Description |
| ---- | ------ | ----------- |
| DR_UNDEFINED | 0 |  |
| SF7 | 2 |  |
| SF8 | 4 |  |
| SF9 | 8 |  |
| SF10 | 16 |  |
| SF11 | 32 |  |
| SF12 | 64 |  |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

