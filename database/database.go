package database

import (
	"sync"

	"gitlab.com/loranna/venvironment/uplink"
)

type Database struct {
	devices           map[string]uplink.DeviceMetadata
	gateways          map[string]uplink.GatewayMetadata
	devmutex, gwmutex sync.Mutex
}

func New() *Database {
	db := Database{}
	db.devices = make(map[string]uplink.DeviceMetadata)
	db.gateways = make(map[string]uplink.GatewayMetadata)
	db.devmutex = sync.Mutex{}
	db.gwmutex = sync.Mutex{}
	return &db
}

func (db *Database) RemoveDevice(deveui string) {
	db.devmutex.Lock()
	defer db.devmutex.Unlock()
	delete(db.devices, deveui)
}

func (db *Database) ListDevices() []uplink.DeviceMetadata {
	db.devmutex.Lock()
	defer db.devmutex.Unlock()
	devs := make([]uplink.DeviceMetadata, len(db.devices))
	i := 0
	for _, dev := range db.devices {
		devs[i] = dev
		i++
	}
	return devs
}

func (db *Database) UpdateDevice(d uplink.DeviceMetadata) error {
	db.devmutex.Lock()
	defer db.devmutex.Unlock()
	db.devices[d.Deveui] = d
	return nil
}

func (db *Database) RemoveGateway(gwmac string) {
	db.gwmutex.Lock()
	defer db.gwmutex.Unlock()
	delete(db.gateways, gwmac)
}

func (db *Database) ListGateways() []uplink.GatewayMetadata {
	db.gwmutex.Lock()
	defer db.gwmutex.Unlock()
	gws := make([]uplink.GatewayMetadata, len(db.gateways))
	i := 0
	for _, gw := range db.gateways {
		gws[i] = gw
		i++
	}
	return gws
}

func (db *Database) UpdateGateway(g uplink.GatewayMetadata) error {
	db.gwmutex.Lock()
	defer db.gwmutex.Unlock()
	db.gateways[g.GetMacaddress()] = g
	return nil
}
