# CHANGELOG

- Lots of minor and major modifications

## 0.8.78

- expand documentation

## 0.7.68

- move protocol into this project

## 0.6.61

- add documentation

## 0.6.53

- remove flags support: `loriotserverurl`, `loriotapikey`, `loriotapplicationidhex`, `loriotnetworkidhex`, `loriotignore`
- remove device registration to LORIOT

## 0.5.42

- add flags support: `loriotserverurl`, `loriotapikey`, `loriotapplicationidhex`, `loriotnetworkidhex`, `loriotignore`
- add device registration to LORIOT

## 0.4.31

- more verbose logs
- various bugfixes
- reads a rabbitmq queue for downlinks

## 0.3.24

- more verbose logs
- various bugfixes

## 0.2.11

- replace mongodb with in-memory database
- use individual queues instead of fanout exhange
- provide HTTP endpoints to add and remove devices/gateways

## 0.1.5

- reads a rabbitmq queue for uplinks
- sets fix radio parameters for all uplinks
- track devices and gateways in a mongodb
