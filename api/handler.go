package api

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/venvironment/database"
)

type Handlers struct {
	logger       *logrus.Logger
	errorChannel chan<- error
	db           *database.Database
}

func (h *Handlers) Logger(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		defer h.logger.Printf("request processed in %s\n", time.Since(startTime))
		next(w, r)
	}
}
func (h *Handlers) SetupRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/status", h.status)
	mux.HandleFunc("/device/add", h.deviceAdd)
	mux.HandleFunc("/device/remove", h.deviceRemove)
	mux.HandleFunc("/device/list", h.deviceList)
	mux.HandleFunc("/gateway/add", h.gatewayAdd)
	mux.HandleFunc("/gateway/remove", h.gatewayRemove)
	mux.HandleFunc("/gateway/list", h.gatewayList)
}

//NewHandlers creates a new Handlers struct
func NewHandlers(logger *logrus.Logger, errchannel chan<- error, db *database.Database) *Handlers {
	return &Handlers{
		logger:       logger,
		errorChannel: errchannel,
		db: db,
	}
}

func (h *Handlers) status(w http.ResponseWriter, r *http.Request) {
	// if webserver is running then everything is up, does not really matter what is returned here
	w.WriteHeader(http.StatusOK)
}
