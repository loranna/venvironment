package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gogo/protobuf/proto"
	"gitlab.com/loranna/venvironment/uplink"
)

func (h *Handlers) deviceAdd(w http.ResponseWriter, r *http.Request) {
	var resd uplink.DeviceMetadata
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("%v", err)
		h.errorChannel <- err
		return
	}
	defer r.Body.Close()

	err = proto.Unmarshal(bodyBytes, &resd)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("%v", err)
		h.errorChannel <- err
		return
	}
	h.logger.Infof("adding device to local db %s with groups: %v", resd.GetDeveui(), resd.GetGroups())
	err = h.db.UpdateDevice(resd)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("%v", err)
		h.errorChannel <- err
		return
	}
}

func (h *Handlers) deviceRemove(w http.ResponseWriter, r *http.Request) {
	var resd uplink.DeviceMetadata
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("could not ioutil.ReadAll: %v", err)
		h.errorChannel <- err
		return
	}
	defer r.Body.Close()

	err = proto.Unmarshal(bodyBytes, &resd)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("could not proto.Unmarshal: %v", err)
		h.errorChannel <- err
		return
	}
	h.logger.Infof("removing deveui %s from local database", resd.GetDeveui())
	h.db.RemoveDevice(resd.GetDeveui())
}

func (h *Handlers) deviceList(w http.ResponseWriter, r *http.Request) {
	devices := h.db.ListDevices()
	devs, err := json.Marshal(devices)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("could not json.Unmarshal: %v", err)
		h.errorChannel <- err
		return
	}
	_, err = w.Write(devs)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("could not w.Write: %v", err)
		h.errorChannel <- err
		return
	}
}
