package api

import (
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gogo/protobuf/proto"
	"gitlab.com/loranna/venvironment/uplink"
)

func (h *Handlers) gatewayAdd(w http.ResponseWriter, r *http.Request) {
	var resg uplink.GatewayMetadata
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("ioutil.ReadAll failed: %v", err)
		h.errorChannel <- err
		return
	}
	defer r.Body.Close()
	bbodyBytes, err := hex.DecodeString(string(bodyBytes))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("hex.DecodeString failed: %v", err)
		h.errorChannel <- err
		return
	}
	err = proto.Unmarshal(bbodyBytes, &resg)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("proto.Unmarshal failed: %v", err)
		h.errorChannel <- err
		return
	}
	h.logger.Infof("adding gateway to local database %s with groups: %v", resg.GetMacaddress(), resg.GetGroups())
	err = h.db.UpdateGateway(resg)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("%v", err)
		h.errorChannel <- err
		return
	}
}

func (h *Handlers) gatewayRemove(w http.ResponseWriter, r *http.Request) {
	var resg uplink.GatewayMetadata
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("ioutil.ReadAll failed: %v", err)
		h.errorChannel <- err
		return
	}
	defer r.Body.Close()
	bbodyBytes, err := hex.DecodeString(string(bodyBytes))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("hex.DecodeString failed: %v", err)
		h.errorChannel <- err
		return
	}
	err = proto.Unmarshal(bbodyBytes, &resg)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("proto.Unmarshal failed: %v", err)
		h.errorChannel <- err
		return
	}
	h.logger.Printf("removing gateway %v", resg)
	h.db.RemoveGateway(resg.GetMacaddress())
}

func (h *Handlers) gatewayList(w http.ResponseWriter, r *http.Request) {
	gateways := h.db.ListGateways()
	gws, err := json.Marshal(gateways)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("could not json.Unmarshal: %v", err)
		h.errorChannel <- err
		return
	}
	_, err = w.Write(gws)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Errorf("could not w.Write: %v", err)
		h.errorChannel <- err
		return
	}
}
