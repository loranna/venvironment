module gitlab.com/loranna/venvironment

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.3.1
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	gitlab.com/loranna/configuration v0.0.0-20200525211328-0e02b38a1927
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
