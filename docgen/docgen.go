package main

import (
	"gitlab.com/loranna/venvironment/cmd"
)

func main() {
	err := cmd.GenerateDocumentation()
	if err != nil {
		panic(err)
	}
}
