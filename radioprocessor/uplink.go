package radioprocessor

import (
	"strings"

	"gitlab.com/loranna/venvironment/uplink"
)

type RadioProcessor struct {
	devices  []uplink.DeviceMetadata
	gateways []uplink.GatewayMetadata
}

func New(devices []uplink.DeviceMetadata, gateways []uplink.GatewayMetadata) *RadioProcessor {
	rp := RadioProcessor{}
	rp.devices = devices
	rp.gateways = gateways
	return &rp
}

func (rp *RadioProcessor) ProcessUplink(d uplink.WithMetadataFromDevice) map[string]uplink.WithMetadataToGateway {
	uplinks := make(map[string]uplink.WithMetadataToGateway, len(rp.gateways))
	for _, gateway := range rp.gateways {
		u := uplink.WithMetadataToGateway{}
		u.Payload = d.GetPayload()
		u.Frequency = d.GetFrequency()
		u.Modulation = d.GetModulation()
		u.Bandwidth = d.GetBandwidth()
		u.Spreadingfactor = d.GetSpreadingfactor()
		u.Coderate = d.GetCoderate()
		u.Payloadsize = d.GetPayloadsize()
		u.Signalstrength = d.GetTransmitsignalstrength()
		samegroup := rp.isSameGroup(d.Deveui, gateway.GetMacaddress())
		if samegroup {
			uplinks[gateway.GetMacaddress()] = u
		}
	}
	return uplinks
}

func (rp *RadioProcessor) isSameGroup(deveui, macaddress string) bool {
	for _, device := range rp.devices {
		if strings.ToLower(device.Deveui) == strings.ToLower(deveui) {
			devgroups := device.GetGroups()
			for _, gateway := range rp.gateways {
				if strings.ToLower(gateway.Macaddress) == strings.ToLower(macaddress) {
					gwgroups := gateway.GetGroups()
					return hasIntersection(devgroups, gwgroups)
				}
			}
		}
	}
	return false
}

func hasIntersection(aa, bb []uint32) bool {
	for _, a := range aa {
		for _, b := range bb {
			if a == b {
				return true
			}
		}
	}
	return false
}

func (rp *RadioProcessor) groupsofgateway(d uplink.WithMetadataFromGateway) []uint32 {
	for _, gateway := range rp.gateways {
		if gateway.GetMacaddress() == d.GetMacaddress() {
			return gateway.Groups
		}
	}
	// by default deveui is member of the group 0
	return []uint32{0}
}

func (rp *RadioProcessor) ProcessDownlink(d uplink.WithMetadataFromGateway) map[string]uplink.WithMetadataToDevice {
	downlinks := make(map[string]uplink.WithMetadataToDevice, len(rp.devices))
	for _, device := range rp.devices {
		u := uplink.WithMetadataToDevice{}
		u.Payload = d.GetPayload()
		u.Frequency = d.GetFrequency()
		u.Modulation = d.GetModulation()
		u.Bandwidth = d.GetBandwidth()
		u.Spreadingfactor = d.GetSpreadingfactor()
		u.Coderate = d.GetCoderate()
		u.Payloadsize = d.GetPayloadsize()
		u.Signalstrength = d.GetTransmitsignalstrength()
		samegroup := rp.isSameGroup(device.GetDeveui(), d.GetMacaddress())
		if samegroup {
			downlinks[device.GetDeveui()] = u
		}
	}
	return downlinks
}
