# Environmentd

Environmentd is a CLI that simulates a radio environment and modifies all messages' radio parameters

# Commands

- [environment](#environment)
  - [start](#environment-start)
  - [version](#environment-version)
