# build stage
FROM golang:1.13 AS build
ADD go.mod /app/
WORKDIR /app

ARG GOPROXY
ARG MAJOR
ARG MINOR
ARG COMMITCOUNT
ARG GONOSUMDB

ENV CGO_ENABLED=0
RUN go mod download
ADD . /app
RUN ./scripts/build.sh

# final stage
FROM ubuntu:18.04
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install ca-certificates -y --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
COPY --from=build /app/venvironment /venvironment
EXPOSE 35999
COPY wait-for-it.sh /wait-for-it.sh
ENTRYPOINT ["/venvironment"]
CMD ["version"]
