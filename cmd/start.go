package cmd

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/loranna/configuration/environment"
	"gitlab.com/loranna/venvironment/api"
	"gitlab.com/loranna/venvironment/broker"
	"gitlab.com/loranna/venvironment/database"
)

var v = viper.New()

func init() {
	v.AllowEmptyEnv(true)
	environment.AddAndBindFlags(startCmd, v)
	environment.BindEnv(v)
	rootCmd.AddCommand(startCmd)
}

func starthttpserver(err chan<- error, server *http.Server) {
	e := server.ListenAndServe()
	if e != http.ErrServerClosed {
		err <- e
	}
}

var errorChannel = make(chan error, 100)

var startCmd = &cobra.Command{
	Use:     "start",
	Short:   "starts simulated environment",
	Example: "environment start",
	RunE: func(cmd *cobra.Command, args []string) error {
		c, err := environment.LoadConfig(v)
		if err != nil {
			return err
		}
		db := database.New()
		br := broker.New(c.RabbitMQUsername, c.RabbitMQPassword, c.RabbitMQUrl, c.RabbitMQPort, errorChannel, db)
		err = br.Connect()
		if err != nil {
			return err
		}
		defer br.Close()
		err = br.DeclareQueues(c.Deviceupqueuename, c.Gatewaydownqueuename)
		if err != nil {
			return err
		}

		br.StartConsume()

		h := api.NewHandlers(logrus.StandardLogger(), errorChannel, db)
		mux := http.NewServeMux()
		h.SetupRoutes(mux)
		addr := fmt.Sprintf("%s:%d", c.ServerURL, c.Serverport)
		server := &http.Server{Addr: addr, Handler: mux}

		go starthttpserver(errorChannel, server)
		defer server.Shutdown(context.Background())
		logrus.Infof("environment started listening on %s", addr)

		ch := make(chan os.Signal, 1)
		signal.Notify(ch, os.Interrupt)
		select {
		case <-ch:
			return nil
		case e := <-errorChannel:
			return e
		}
	},
}
