package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "environment",
	Short: "simulated environment",
	Long: `environment simulates a virtual radio environment and
modifies all messages' radio parameters`,
	DisableAutoGenTag: true,
	SilenceErrors:     false,
	SilenceUsage:      true,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
