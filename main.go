package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/venvironment/cmd"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	cmd.Execute()
}
