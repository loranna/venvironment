package broker

import (
	"fmt"

	"github.com/gogo/protobuf/proto"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/loranna/venvironment/database"
	"gitlab.com/loranna/venvironment/radioprocessor"
	"gitlab.com/loranna/venvironment/uplink"
)

type Broker struct {
	username, password, url string
	port                    uint
	conn                    *amqp.Connection
	channelDeviceUp         *amqp.Channel
	channelEnvironmentUp    *amqp.Channel
	channelEnvironmentDown  *amqp.Channel
	channelGatewayDown      *amqp.Channel

	devupqueuename, gwdnqueuename string

	db           *database.Database
	errorChannel chan<- error
}

func New(username, password, url string, port uint, errorChannel chan<- error, db *database.Database) *Broker {
	broker := Broker{}
	broker.username = username
	broker.password = password
	broker.url = url
	broker.port = port
	broker.errorChannel = errorChannel
	broker.db = db
	return &broker
}

func (b *Broker) Connect() error {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%d/", b.username, b.password, b.url, b.port))
	if err != nil {
		return fmt.Errorf("amqp.Dial failed, %w", err)
	}
	b.conn = conn
	b.channelDeviceUp, err = conn.Channel()
	if err != nil {
		return err
	}
	errdevupchan := make(chan *amqp.Error, 0)
	b.channelDeviceUp.NotifyClose(errdevupchan)
	go b.forwardtoerrchan(errdevupchan)
	b.channelEnvironmentUp, err = conn.Channel()
	if err != nil {
		return err
	}
	errenvupchan := make(chan *amqp.Error, 0)
	b.channelEnvironmentUp.NotifyClose(errenvupchan)
	go b.forwardtoerrchan(errenvupchan)
	b.channelEnvironmentDown, err = conn.Channel()
	if err != nil {
		return err
	}
	errenvdnchan := make(chan *amqp.Error, 0)
	b.channelEnvironmentDown.NotifyClose(errenvdnchan)
	go b.forwardtoerrchan(errenvdnchan)
	b.channelGatewayDown, err = conn.Channel()
	if err != nil {
		return err
	}
	errgwdnchan := make(chan *amqp.Error, 0)
	b.channelGatewayDown.NotifyClose(errgwdnchan)
	go b.forwardtoerrchan(errgwdnchan)
	return nil
}

func (b *Broker) forwardtoerrchan(cc chan *amqp.Error) {
	for c := range cc {
		logrus.Errorf("channel error happened: %s", c.Error())
		b.errorChannel <- c
	}
}

func (b *Broker) Close() error {
	// consume ranges are closed by the conn.Close
	// see end of example: https://godoc.org/github.com/streadway/amqp#example-Channel-Consume
	return b.conn.Close()
}

func (b *Broker) DeclareQueues(devupname, gwdnname string) error {
	_, err := b.channelDeviceUp.QueueDeclare(
		devupname, // name
		false,     // durable
		true,      // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		return err
	}
	b.devupqueuename = devupname
	_, err = b.channelGatewayDown.QueueDeclare(
		gwdnname, // name
		false,    // durable
		true,     // delete when unused
		false,    // exclusive
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		return err
	}

	b.gwdnqueuename = gwdnname
	return nil
}

func (b *Broker) StartConsume() {
	go b.consumeDeviceUp(b.errorChannel)
	go b.consumeGatewayDown(b.errorChannel)
}

func (b *Broker) consumeDeviceUp(errorChannel chan<- error) {
	deviceup, err := b.channelDeviceUp.Consume(
		b.devupqueuename, // queue
		"",               // consumer
		false,            // auto-ack
		false,            // exclusive
		false,            // no-local
		false,            // no-wait
		nil,              // args
	)
	if err != nil {
		errorChannel <- err
		return
	}
	for d := range deviceup {
		u := &uplink.WithMetadataFromDevice{}

		err := proto.Unmarshal(d.Body, u)
		if err != nil {
			logrus.Warnf("could not unmarshal device message: %X", d.Body)
			d.Ack(false)
			continue
		}
		logrus.Debugf("uplink from %s received", u.GetDeveui())

		rp := radioprocessor.New(b.db.ListDevices(), b.db.ListGateways())
		uplinks := rp.ProcessUplink(*u)
		for gweui, uplink := range uplinks {
			uplinkbytes, err := proto.Marshal(&uplink)
			if err != nil {
				logrus.Warnf("could not marshal message: %v", uplink)
				d.Ack(false)
				continue
			}
			rkey := "gw" + gweui
			//queue declared on db update
			err = b.channelEnvironmentUp.Publish(
				"",    // exchange
				rkey,  // routing key
				false, // mandatory
				false, // immediate
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        uplinkbytes,
				})
			if err != nil {
				logrus.Warnf("could not publish message: %s to queue: %s", string(uplinkbytes), rkey)
				d.Ack(false)
				continue
			}
		}
		d.Ack(false)
	}
}

func (b *Broker) consumeGatewayDown(errorChannel chan<- error) {
	gatewaydown, err := b.channelGatewayDown.Consume(
		b.gwdnqueuename, // queue
		"",              // consumer
		false,           // auto-ack
		false,           // exclusive
		false,           // no-local
		false,           // no-wait
		nil,             // args
	)
	if err != nil {
		errorChannel <- err
		return
	}
	for d := range gatewaydown {
		u := &uplink.WithMetadataFromGateway{}
		err := proto.Unmarshal(d.Body, u)
		if err != nil {
			logrus.Warnf("could not unmarshal gateway message: %+v", d)
			d.Ack(false)
			continue
		}
		logrus.Debugf("downlink from %s received", u.GetMacaddress())

		rp := radioprocessor.New(b.db.ListDevices(), b.db.ListGateways())
		dls := rp.ProcessDownlink(*u)
		for deveui, dl := range dls {
			dlbytes, err := proto.Marshal(&dl)
			if err != nil {
				logrus.Warnf("could not marshal message: %v", dlbytes)
				d.Ack(false)
				continue
			}
			rkey := "dev" + deveui
			//queue declared on db update
			err = b.channelEnvironmentDown.Publish(
				"",    // exchange
				rkey,  // routing key
				false, // mandatory
				false, // immediate
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        dlbytes,
				})
			if err != nil {
				logrus.Warnf("could not publish message: %s to queue: %s", string(dlbytes), rkey)
				d.Ack(false)
				continue
			}
		}
		d.Ack(false)
	}
}
