## environment

simulated environment

### Synopsis

environment simulates a virtual radio environment and
modifies all messages' radio parameters

### Options

```
  -h, --help   help for environment
```

### SEE ALSO

* [environment start](#environment-start)	 - starts simulated environment
* [environment version](#environment-version)	 - Show version number

