## environment version

Show version number

### Synopsis

Show version number

```
environment version [flags]
```

### Options

```
  -h, --help   help for version
```

### SEE ALSO

* [environment](#environment)	 - simulated environment

