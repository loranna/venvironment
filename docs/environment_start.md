## environment start

starts simulated environment

### Synopsis

starts simulated environment

```
environment start [flags]
```

### Examples

```
environment start
```

### Options

```
      --deviceupqueuename string      Name of the queue used for the devie uplink to the environment messages (default "uplinktoenvironment")
      --environmentserverport int     Port of the environment used to manipulate messages in both directions (default 35999)
      --environmentserverurl string   URL of the environment used to manipulate messages in both directions
      --gatewaydownqueuename string   Name of the queue used for the gateway downlink to the environment messages (default "downlinktoenvironment")
  -h, --help                          help for start
      --rabbitmqpassword string       Password of the RabbitMQ server used for data exchange (default "guest")
      --rabbitmqport int              Port of the RabbitMQ server used for data exchange (default 5672)
      --rabbitmqserverurl string      URL of the RabbitMQ server used for data exchange (default "localhost")
      --rabbitmqusername string       User Name of the RabbitMQ server used for data exchange (default "guest")
```

### SEE ALSO

* [environment](#environment)	 - simulated environment

