#!/bin/bash

docker run --rm -it -v $(pwd):/p -w /p tkiraly/protoc-builder:latest \
--go_out=. uplink.proto
mkdir -p uplink
mv -f uplink.pb.go uplink/
docker run --rm -it -v $(pwd):/p -w /p tkiraly/protoc-builder:latest \
--c_out=. uplink.proto
mv -f uplink.pb-c.c ../vgateway/libloragw/src
mv -f uplink.pb-c.h ../vgateway/libloragw/inc